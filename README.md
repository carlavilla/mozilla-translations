# Mozilla Hispano traductions

Traductions from Mozilla Hacks that I've made for the blog of Mozilla Hispano in the Labs section. 

## Mozilla Hispano Labs
Link for Mozilla Hispano labs: https://www.mozilla-hispano.org/labs/

## List of traductions

* https://www.mozilla-hispano.org/quantum-de-cerca-motor-de-navegador-web/
* https://www.mozilla-hispano.org/webgl-2-disponible-en-firefox/
* https://www.mozilla-hispano.org/webvr-para-todos-los-usuarios-de-windows/
* https://www.mozilla-hispano.org/web-push-llega-a-firefox-44/
* https://www.mozilla-hispano.org/serie-de-videos-de-desarrollo-de-juegos-html5/
* https://www.mozilla-hispano.org/optimizando-el-rendimiento-de-javascript-con-firefox/
* https://www.mozilla-hispano.org/que-quieres-de-tus-herramientas-de-desarrollo/
* https://www.mozilla-hispano.org/tu-pagina-de-facebook-aplicacion-de-firefox-os/
* https://www.mozilla-hispano.org/modelo-de-cajas-css-para-elementos-en-linea/
* https://www.mozilla-hispano.org/pseudo-elementos-inspeccion-de-promesas-cabeceras-sin-procesar-y-mucho-mas-firefox-developer-edition-36/

## Tools

* [Mozilla Firefox] (https://www.mozilla.org/en-US/firefox/new/)
* [Calligra] (https://www.calligra.org/)
* [Plasma Desktop] (https://www.kde.org/plasma-desktop)
* [Kate] (https://kate-editor.org/)
